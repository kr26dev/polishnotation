import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws PolishNotation.IncorrectInputException {

        // Входная строка

        System.out.print("Введите выражение: ");
        Scanner scanner = new Scanner(System.in);
        String in = scanner.nextLine();

        // Создаем новый экземпляр класса

        PolishNotation polish = new PolishNotation();

        // Вывод инфиксной записи
        ArrayList<String> infix = polish.CheckInput(polish.Parse(in));

        System.out.print("Инфиксная запись: ");
        for (String s:infix) { System.out.print(s);  }

        // Вывод постфиксной записи (обратной польской заиписи)
        ArrayList<String> postfix = polish.PolishRecord(infix);

        System.out.print("\nПостфиксная запись: ");
        for (String s:postfix) { System.out.print(s+" ");  }

        // Вычисляем и выводим результат на экран
        System.out.println("\nРезультат вычисления: "+polish.Calculate(postfix));

    }
}

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * Created by Admin on 09.06.2017.
 */

public class PolishNotation {

    // Проверка принадлежности оператра к операторам низшего приоритета

    private boolean isSecondary (String operator)
    {
        HashSet<String> slaveoper = new HashSet<>();
        slaveoper.add("+");
        slaveoper.add("-");

        return slaveoper.contains(operator);
    }

    // Проверка принадлежности оператра к операторам высшего приоритета

    private boolean isPrimary (String operator)
    {
        HashSet<String> mainoper = new HashSet<>();
        mainoper.add("*");
        mainoper.add("/");

        return mainoper.contains(operator);
    }

    // Проверка принадлежности оператра к математическим операторам (+-*/)

    private boolean isMathsOperator (String operator)
    {
        HashSet<String> mathsoper = new HashSet<>();
        mathsoper.add("+");
        mathsoper.add("-");
        mathsoper.add("*");
        mathsoper.add("/");

        return mathsoper.contains(operator);
    }


    // Парсинг входной строки, удаляем все лишнее, разбиваем на токены

    public ArrayList<String> Parse(String in) {

        ArrayList<String> arr = new ArrayList<>();

        String token;

        StringTokenizer st = new StringTokenizer(in, " +-*/()",true);

        while (st.hasMoreTokens()) {

        token = st.nextToken();

        if (!token.equals(" ")) arr.add(token);

        }
        return arr;
    }

    // Создаем свое исключение, обрабатывающее некорректный ввод
    public class IncorrectInputException extends Exception {

        IncorrectInputException() { }

        IncorrectInputException(String msg) {
            super(msg);
        }

    }

    // Проверка массива с токенами на наличие неподдерживаемых операторов или их сочетаний

    public ArrayList<String> CheckInput(ArrayList arr) throws IncorrectInputException
    {
        // Последний проверенный токен
        String last="";

        // Проверка на отсутствие операторов в выражении и на пустую строку
        if (!arr.contains("+") & !arr.contains("-") & !arr.contains("*") & !arr.contains("/"))
            throw new IncorrectInputException("Некорректный ввод! Арифметические операторы не обнаружены.");


        for (int i=0;i<arr.size();i++) {

            String op = (String) arr.get(i);

            // Проверка первого и последнего символа в выражении
            if (i==0 | i==(arr.size()-1)) {
                if (isMathsOperator(op))
                    throw new IncorrectInputException("Некорректный ввод! Проверьте арифметические операторы и операнды.");
            }

            // Проверка на наличие неподдерживаемых символов
            if (!op.matches("\\d+") & !isMathsOperator(op) & !op.contains("."))
                throw new IncorrectInputException("Некорректный ввод! В выражении содержатся неподдерживаемые символы: "+op);

            // Проверка на два операнда подряд
            if (op.matches("\\d+") & last.matches("\\d+"))
                throw new IncorrectInputException("Некорректный ввод! Проверьте арифметические операторы между операндами: "+last+" "+op);

            // Проверка на два арифметических оператора подряд
            if (isMathsOperator(op) & isMathsOperator(last))
                    throw new IncorrectInputException("Некорректный ввод! Проверьте арифметические операторы: "+last+op);

            // Проверка на начало операнда с точки
            if (op.charAt(0)=='.')
                throw new IncorrectInputException("Некорректный ввод! Операнд не может начинаться с точки: "+op);

            // Проверка на деление на ноль
            if (op.equals("0") & last.equals("/"))
                    throw new IncorrectInputException("Некорректный ввод! Делить на ноль нельзя.");

            last = op;

        }

      return arr;
    }

    // Перевод инфиксной записи в обратную польскую запись
    // Используем стек для реализации алгоритма сортировочной станции

    public ArrayList<String> PolishRecord (ArrayList arr) {

        ArrayList<String> out = new ArrayList<>();

        // Последний помещенный в стек арифметический оператор
        String last = "";


        Stack<String> stackOper = new Stack<>();

        for (int i=0;i<arr.size();i++)
        {
            String elem = (String) arr.get(i);

            // Если элемент является арифм. оператором, то:

            if (isMathsOperator(elem))
            {

                // Если стек не пуст
                if (!stackOper.isEmpty()) {

                    // Если на вершине стека оператор с большим или таким же приоритетом
                    // выталкиваем из стека в выходной массив
                    if ((isSecondary(elem) & isPrimary(last)) | (isSecondary(last) & isSecondary(elem)))
                    { while(!stackOper.isEmpty()) out.add(stackOper.pop()); }
                }

                    // Если в стеке ничего нет, или на вершине стека оператор с меньшим приоритетом,
                    // просто добавляем его в стек

                    stackOper.push(elem);
                    last = elem;

            }

            // Иначе если это число
            else  out.add(elem);

        }

        // Выдавливаем все операторы из стека в выходной массив

        while(!stackOper.isEmpty()) out.add(stackOper.pop());

        return out;
    }


    // Производим вычисления из обратной польской записи

    public float Calculate(ArrayList str) throws ArithmeticException {

        Stack<Float> res = new Stack();


        for (int i=0;i<str.size();i++) {

            String op = (String) str.get(i);

            if (isMathsOperator(op)) {

                float last = res.pop(); // Вершина стека, последний пришедший
                float second = res.pop(); // Предпоследний пришедший

                switch (op) {

                    case "+": res.push(second+last);
                        break;
                    case "-": res.push(second-last);
                        break;
                    case "*": res.push(second*last);
                        break; // Запасная проверка деления на ноль, если чекер пропустил:
                    case "/": if (last!=0) res.push(second/last);
                    else throw new ArithmeticException("Ошибка! Делить на ноль нельзя!");
                        break;
                }

            }

            else
            {
                try { res.push(Float.valueOf(op)); }
                catch (NumberFormatException e) { System.err.println("Неверный формат строки!"); }
            }

        }

        return res.pop();
    }


}
